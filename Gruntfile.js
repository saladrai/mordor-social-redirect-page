module.exports = function (grunt) {
    grunt.initConfig({
        cssmin: {
            dist: {
                files: {
                    'styles.min.css': './styles.css'
                }
            }
        },
        watch: {
            files: ['./styles.css'],
            tasks: ['cssmin']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['cssmin']);
};
